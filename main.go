package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

// MajorVersion :
var MajorVersion = "0"

// MinorVersion :
var MinorVersion = "1"

func main() {
	fmt.Print("Knockout CLI ")
	fmt.Println(MajorVersion + "+" + MinorVersion)
	fmt.Println("Making the console higher is recommended!")

	// if the user is supplying arguments
	// treat them as a command else load subforums
	if len(os.Args[1:]) != 0 {
		executeCommand(strings.Join(os.Args[1:], " "))
	} else {
		fmt.Print("Getting subforums...")
		subforums, err := getSubforums()
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println("done")

		fmt.Println(FormatSubforums(subforums))
		fmt.Println(`Use "subforum id" to access a subforum`)
	}

	for true {
		reader := bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		executeCommand(text)
	}
}
