package main

import (
	"encoding/json"
	"fmt"
)

// GenerateSlateObject : Converts a json string into the custom slate class
func GenerateSlateObject(data string) (Slate, error) {
	var slate Slate
	err := json.Unmarshal([]byte(data), &slate)
	if err != nil {
		fmt.Println(err)
		return slate, err
	}
	return slate, nil
}

// Slate : class for
type Slate struct {
	Object   string        `json:"object"`
	Document SlateDocument `json:"document"`
}

// SlateDocument : class for
type SlateDocument struct {
	Object string `json:"object"`
	//Data   string      `json:"data"`
	Nodes []SlateNode `json:"nodes"`
}

// SlateNode : class for
type SlateNode struct {
	Object string       `json:"object"`
	Type   string       `json:"type"`
	Data   Data         `json:"data"`
	Nodes  []SlateNode  `json:"nodes"`
	Leaves []SlateLeave `json:"leaves"`
}

// SlateLeave : class for
type SlateLeave struct {
	Object string `json:"object"`
	Text   string `json:"text"`
	//Marks  string `json:"marks"`
}

// Data : dumb
type Data struct {
	Src string `json:"src"`
}
