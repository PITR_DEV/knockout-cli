# Knockout CLI
![Knockout CLI](https://i.imgur.com/pA3nSAm.gif)
The gimmickiest of the gimmicks

## Gallery
![Screenshot 1](https://api.underscorestudios.net/img/5babdb00babce94b2020b371/01958560da166b25/A9ugTPAS4.webp)
## Setup

Requirements:
- Go

Running:
- Make sure that Go is in your path
- Run `run.sh` or `run.bat`

Building:
- Make sure that Go is in your path
- Run `build.sh` or `build.bat`

## Contribution

- Make a branch for everything you do.
- Use "branch folders" f.e. `feature/my-branch`
- Create a pull request once the feature is complete