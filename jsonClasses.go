package main

// Subforums : for converting api responses
type Subforums struct {
	List []Subforum `json:"list"`
}

// Subforum : universal class for subforums
type Subforum struct {
	ID      int            `json:"id"`
	Name    string         `json:"name"`
	Threads []BorkedThread `json:"threads"`
}

// BorkedThread : when listing threads, api returns different crap
type BorkedThread struct {
	ID     int    `json:"id"`
	Title  string `json:"title"`
	Posts  int    `json:"postCount"`
	Locked int    `json:"locked"`
	Pinned int    `json:"pinned"`
}

// Thread : universal class for threads
type Thread struct {
	ID        int    `json:"id"`
	User      []User `json:"user"`
	Title     string `json:"title"`
	PostCount int    `json:"postCount"`
	Posts     []Post `json:"posts"`
	Page      int    `json:"currentPage"`
	Deleted   bool   `json:"deleted"`
	Locked    bool   `json:"locked"`
	Pinned    bool   `json:"pinned"`
}

// User : universal class for users
type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Group    int    `json:"usergroup"`
}

// Post : class for
type Post struct {
	ID         int    `json:"id"`
	CreatedAt  string `json:"createdAt"`
	User       User   `json:"user"`
	RawContent string `json:"content"`
	Content    Slate
}
