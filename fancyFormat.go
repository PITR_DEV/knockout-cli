package main

import (
	"strconv"
	"strings"
)

// FormatSubforums :
func FormatSubforums(subforums []Subforum) string {
	var stringBuilder strings.Builder
	for index, element := range subforums {
		stringBuilder.WriteString(strconv.Itoa(index))
		stringBuilder.WriteString(" - ")
		stringBuilder.WriteString(element.Name)
		stringBuilder.WriteString("\n")
	}
	return stringBuilder.String()
}

// FormatThreads :
func FormatThreads(threads []BorkedThread) string {
	var stringBuilder strings.Builder
	for _, element := range threads {
		stringBuilder.WriteString(strconv.Itoa(element.ID))
		stringBuilder.WriteString(" - ")
		stringBuilder.WriteString(element.Title)
		stringBuilder.WriteString("\n")
	}
	return stringBuilder.String()
}

// FormatThread :
func FormatThread(thread Thread) string {
	var stringBuilder strings.Builder
	stringBuilder.WriteString("Thread: ")
	stringBuilder.WriteString(thread.Title)
	stringBuilder.WriteString(" by ")
	stringBuilder.WriteString(thread.User[0].Username)
	stringBuilder.WriteString("\n")
	if thread.Locked || thread.Pinned || thread.Deleted {
		if thread.Locked {
			stringBuilder.WriteString("Locked ")
		}
		if thread.Pinned {
			stringBuilder.WriteString("Pinned ")
		}
		if thread.Deleted {
			stringBuilder.WriteString("Deleted")
		}
		stringBuilder.WriteString("\n")
	}
	stringBuilder.WriteString("Page: ")
	stringBuilder.WriteString(strconv.Itoa(thread.Page))
	stringBuilder.WriteString("\n\n")
	for _, element := range thread.Posts {
		stringBuilder.WriteString(element.User.Username)
		stringBuilder.WriteString(":\n")
		content := RenderSlate(element.Content)
		stringBuilder.WriteString(content)
		stringBuilder.WriteString("\n\n\n")
	}

	stringBuilder.WriteString("End of page: ")
	stringBuilder.WriteString(strconv.Itoa(thread.Page))
	stringBuilder.WriteString("\n")

	return stringBuilder.String()
}

// RenderSlate :
func RenderSlate(slate Slate) string {
	var stringBuilder strings.Builder
	for _, node := range slate.Document.Nodes {
		switch node.Type {
		case "heading-one", "heading-two", "paragraph":
			for _, deeperNode := range node.Nodes {
				if deeperNode.Object == "text" {
					stringBuilder.WriteString(RenderLeaves(deeperNode.Leaves))
				}
				if deeperNode.Object == "inline" {
					for _, evenDeeperNode := range deeperNode.Nodes {
						stringBuilder.WriteString(RenderLeaves(evenDeeperNode.Leaves))
					}
				}
			}
			stringBuilder.WriteString("\n")
		case "image":
			stringBuilder.WriteString("[ image:")
			stringBuilder.WriteString(node.Data.Src)
			stringBuilder.WriteString(" ]\n")
		case "youtube":
			stringBuilder.WriteString("[ yt:")
			stringBuilder.WriteString(node.Data.Src)
			stringBuilder.WriteString(" ]\n")
		case "block-quote":
			for _, deeperNode := range node.Nodes {
				stringBuilder.WriteString(" | ")
				stringBuilder.WriteString(RenderLeaves(deeperNode.Leaves))
				stringBuilder.WriteString("\n")
			}
		case "numbered-list", "bulleted-list":
			for _, deeperNode := range node.Nodes {
				for _, evenDeeperNode := range deeperNode.Nodes {
					stringBuilder.WriteString(" * ")
					stringBuilder.WriteString(RenderLeaves(evenDeeperNode.Leaves))
					stringBuilder.WriteString("\n")
				}
			}
		}
	}
	return stringBuilder.String()
}

// RenderLeaves :
func RenderLeaves(leaves []SlateLeave) string {
	var stringBuilder strings.Builder
	for _, leave := range leaves {
		if leave.Text != "" {
			stringBuilder.WriteString(leave.Text)
		} else {
			stringBuilder.WriteString("\n")
		}
	}
	return stringBuilder.String()
}
