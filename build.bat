@echo off
echo Building for win64
set GOOS=windows
set GOARCH=amd64
go build -o build/knockoutcli_64.exe
echo Building for win32
set GOOS=windows
set GOARCH=386
go build -o build/knockoutcli_32.exe
echo Building for linux
set GOOS=linux
set GOARCH=amd64
go build -o build/knockoutcli.linux
echo Building for darwin
set GOOS=darwin
set GOARCH=amd64
go build -o build/knockoutcli.app
set GOOS=windows
set GOARCH=amd64