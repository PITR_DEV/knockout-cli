package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

func executeCommand(str string) {
	// It's called args but element 0 is actually the command
	args := strings.Fields(str)

	if len(args) == 0 {
		fmt.Println(`Type "help" to display available commands`)
		return
	}

	switch args[0] {
	case "subforums", "sbfs":
		subforums, err := getSubforums()
		if err != nil {
			log.Fatalln(err)
			return
		}
		fmt.Println(FormatSubforums(subforums))
	case "subforum", "sbf", "s":
		if !argsCheck(args, 1) {
			return
		}

		cID, err := strconv.Atoi(args[1])
		if err != nil {
			fmt.Println("Failed to process the first argument")
			return
		}

		sbfID := indexToSubforumID(cID)
		if sbfID == -1 {
			fmt.Println("Couldn't find that id in your cache.")
			fmt.Println("Try restarting the CLI")
			return
		}

		page := 1
		if len(args) > 2 {
			page, err = strconv.Atoi(args[2])
			if err != nil {
				page = 1
			}
		}

		subforum, err := getSubforum(sbfID, page)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(FormatThreads(subforum.Threads))
	case "thread", "th", "t":
		if !argsCheck(args, 1) {
			return
		}

		cID, err := strconv.Atoi(args[1])
		if err != nil {
			fmt.Println("Failed to process the first argument")
			return
		}
		page := 1
		if len(args) > 2 {
			page, err = strconv.Atoi(args[2])
			if err != nil {
				page = 1
			}
		}

		thread, err := getThread(cID, page)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(FormatThread(thread))
	case "next", "n":
		page := 1
		progressPage(page)
	case "prev", "p":
		page := 1
		progressPage(-page)
	case "shortcuts", "sh":
		fmt.Println("\nShortcuts:")
		fmt.Println("help, h")
		fmt.Println("shortcuts, sh")
		fmt.Println("subforums, sbfs")
		fmt.Println("subforum, sbf, s")
		fmt.Println("thread, th, t")
		fmt.Println("next, n")
		fmt.Println("prev, p")
	case "help", "h":
		// Hardcoded for now, please don't kill me
		// I'll do it myself
		fmt.Println("\nHelp:")
		fmt.Println("shortcuts - display all possible shortcuts")
		fmt.Println("subforums - redownload subforums")
		fmt.Println("subforum id [page] - display a subforum by it's cached id")
		fmt.Println("thread id [page] - displays a thread by it's actual id")
		fmt.Println("next [amount] - loads the next page on the current thread/subforum")
		fmt.Println("prev [amount] - loads the previous page on the current thread/subforum")
	default:
		fmt.Println("Unknown command.")
		fmt.Println(`Type "help" to display available commands`)
	}
}

func argsCheck(args []string, minArgs int) bool {
	// TODO Add some cool type checks or whatever
	if len(args) > minArgs {
		return true
	}
	fmt.Println("This command requires at least " + strconv.Itoa(minArgs) + " argument[s]")
	return false
}
