package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

var apiURL = "https://api.knockout.chat"
var cachedSubforums []Subforum

// This is dumb, I dislike it
var openThreadID = -1
var openSubforumID = -1
var openPage = 1

func indexToSubforumID(index int) int {
	if len(cachedSubforums) == 0 {
		fmt.Println("No subforums in your cache, downloading...")
		_, err := getSubforums()
		if err != nil {
			log.Fatalln(err)
			return -1
		}
		return cachedSubforums[index].ID
	}
	if index >= len(cachedSubforums) {
		return -1
	}
	return cachedSubforums[index].ID

}

func progressPage(amount int) {
	if openThreadID != -1 {
		if openPage+amount > 0 {
			thread, err := getThread(openThreadID, openPage+amount)
			if err != nil {
				log.Fatalln(err)
				return
			}
			if len(thread.Posts) == 0 {
				fmt.Println("Last page")
				return
			}
			fmt.Println(FormatThread(thread))
		}
	} else if openSubforumID != -1 {
		if openPage+amount > 0 {
			subforum, err := getSubforum(openSubforumID, openPage+amount)
			if err != nil {
				log.Fatalln(err)
				return
			}
			if len(subforum.Threads) == 0 {
				fmt.Println("Last page")
				return
			}
			fmt.Println(FormatThreads(subforum.Threads))
		}
	}
}

func getSubforums() ([]Subforum, error) {
	resp, err := http.Get(apiURL + "/subforum")
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var subforums Subforums
	err = json.Unmarshal(body, &subforums)
	if err != nil {
		return nil, err
	}

	cachedSubforums = subforums.List
	openThreadID = -1
	openSubforumID = -1
	openPage = 1

	return subforums.List, nil
}

func getSubforum(id int, page int) (Subforum, error) {
	resp, err := http.Get(apiURL + "/subforum/" + strconv.Itoa(id))
	var subforum Subforum
	if err != nil {
		return subforum, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return subforum, err
	}

	err = json.Unmarshal(body, &subforum)
	if err != nil {
		return subforum, err
	}

	if len(subforum.Threads) == 0 {
		return subforum, nil
	}

	openThreadID = -1
	openSubforumID = subforum.ID
	openPage = page

	return subforum, nil
}

func getThread(id int, page int) (Thread, error) {
	resp, err := http.Get(apiURL + "/thread/" + strconv.Itoa(id) + "/" + strconv.Itoa(page))
	var thread Thread
	if err != nil {
		return thread, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return thread, err
	}

	err = json.Unmarshal(body, &thread)
	if err != nil {
		return thread, err
	}

	for index, element := range thread.Posts {
		content, err := GenerateSlateObject(element.RawContent)
		if err != nil {
			fmt.Println(err)
			continue
		}
		thread.Posts[index].Content = content
	}

	if len(thread.Posts) == 0 {
		return thread, nil
	}

	openThreadID = id
	openSubforumID = -1
	openPage = page

	return thread, nil
}
