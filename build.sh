#!/bin/sh
echo Building for win64
GOOS=windows
GOARCH=amd64
go build -o build/knockoutcli_64.exe
echo Building for win32
GOOS=windows
GOARCH=386
go build -o build/knockoutcli_32.exe
echo Building for linux
GOOS=linux
GOARCH=amd64
go build -o build/knockoutcli.linux
echo Building for darwin
GOOS=darwin
GOARCH=amd64
go build -o build/knockoutcli.app
GOOS=windows
GOARCH=amd64